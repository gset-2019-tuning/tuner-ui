import React from 'react'
import axios from 'axios'

import '../styles/styles.css'

const string_ids = ['lowe', 'a', 'd', 'g', 'b', 'highe']
const string_names = ['Low E', 'A', 'D', 'G', 'B', 'High E']

var api = axios.create({
  baseURL: "/api/",
  // baseURL: "http://localhost:8080/api/",
})

const StringEntry = ({ dispName, id }) =>
  <tr className="string-line">
    <td className="string-name">{dispName}</td><td>    Pitch: <input type="text" id={`${id}pitch`}></input> <br></br></td>
    <td>Octave: <input type="number" id={`${id}octave`}></input></td>
    <td>
      Cents:&nbsp;
      <select id={`${id}sign`}>
        <option value="+">+</option>
        <option value="-">-</option>
      </select>
      <input type="number" id={`${id}mod`}></input>
    </td>
  </tr>

const App = () => {
  const advNext = () => {
    const enables = string_ids.map((id) =>
      document.getElementById(`${id}enable`).checked
    )
    if (enables.filter(v=>v).length === 1) {
      let i = enables.indexOf(true)
      enables[i] = false
      if (++i < enables.length)
        enables[i] = true
    } else if (enables.filter(v=>v).length === 0) {
      enables[0] = true
    }
    enables.map((v, i) =>
      document.getElementById(`${string_ids[i]}enable`).checked = v
    )
    submitEnables()
  }

  return (
    <>
      <h1 align="center">Guitar Tuner Customization</h1>
      <h3>Enable / Disable Tuners:</h3>
      <form>
          {
            string_ids.map((id, i) =>
              <div><input type="checkbox" id={`${id}enable`}/>{string_names[i]}</div>
            )
          }
      </form>
      <div>
        <button onClick={() => enablesSetAll(true)}>All on</button>
        <button onClick={() => enablesSetAll(false)}>All off</button>
      </div>
      <button onClick={submitEnables}>Submit</button>
      <button onClick={advNext}>Next String</button>
      <br/>
      <h3>Select Note Names: </h3>
      <form>
        <table class="strings-config">
          <tbody>
            {
              string_ids.map((id, i) =>
                <StringEntry dispName={string_names[i]} id={id} />
              )
            }
          </tbody>
        </table>

        <br/>

        <td>Accuracy (cents): <input type="number" id="notecents"></input></td>
      </form>
      <button onClick={storeNotes} type="button">Submit</button>
      

      <br/>
      <h3>Or Select Frequencies:</h3>
      <form background-color="#31c6e4">
        {
          string_ids.map((id, i) =>
            <div>Frequency to Tune {string_names[i]} String to: <input type="number" id={`${id}freq`}></input><br/></div>
          )
        }
        <br/>
        <td>Accuracy (cents): <input type="number" id="freqcents"></input></td>
      </form>
      <button onClick={storeFreqs} type="button">Submit</button>
    
    </>
  )
}

function storeNotes() {
  //Store all values that the user entered
  console.log(document.getElementById('lowepitch').value);
    let lowE = document.getElementById('lowepitch').value;
    let lowECents = document.getElementById('lowemod').value;
    let lowEOctave = document.getElementById('loweoctave').value;
    let a = document.getElementById('apitch').value;
    let aCents = document.getElementById('amod').value;
    let aOctave = document.getElementById('aoctave').value;
    let d = document.getElementById('dpitch').value;
    let dCents = document.getElementById('dmod').value;
    let dOctave = document.getElementById('doctave').value;
    let g = document.getElementById('gpitch').value;
    let gCents = document.getElementById('gmod').value;
    let gOctave = document.getElementById('goctave').value;
    let b = document.getElementById('bpitch').value;
    let bCents = document.getElementById('bmod').value;
    let bOctave = document.getElementById('boctave').value;
    let highE = document.getElementById('highepitch').value;
    let highECents = document.getElementById('highemod').value;
    let highEOctave = document.getElementById('higheoctave').value;
    let cents = document.getElementById('notecents').value;

    //If user selected "-", make the cents value negative
    if(document.getElementById('lowesign').value === "-") {
      lowECents = -lowECents;
    }
    if(document.getElementById('asign').value === "-") {
      aCents = -aCents;
    }
    if(document.getElementById('dsign').value === "-") {
      dCents = -dCents;
    }
    if(document.getElementById('gsign').value === "-") {
      gCents = -gCents;
    }
    if(document.getElementById('bsign').value === "-") {
      bCents = -bCents;
    }
    if(document.getElementById('highesign').value === "-") {
      highECents = -highECents;
    }
    
    //Convert each note to corresponding frequency
    lowE = noteToFrequency(lowE, lowEOctave);
    a = noteToFrequency(a, aOctave);
    d = noteToFrequency(d, dOctave);
    g = noteToFrequency(g, gOctave);
    b = noteToFrequency(b, bOctave);
    highE = noteToFrequency(highE, highEOctave);

    //Add or subtract cents as indicated by the user
    lowE *= Math.pow(2, lowECents/1200);
    a *= Math.pow(2, aCents/1200);
    d *= Math.pow(2, dCents/1200);
    g *= Math.pow(2, gCents/1200);
    b *= Math.pow(2, bCents/1200);
    highE *= Math.pow(2, highECents/1200);

    var allValues = [lowE, a, d, g, b, highE, cents];

    //Send values to Raspberry Pi
    api.put('/values', {
      values: allValues
    });
}

function noteToFrequency(pitch, octave) {
  const notes = {
    "C": 261.63,
    "C#": 277.18,
    "Db": 277.18,
    "D": 293.66,
    "D#": 311.13,
    "Eb": 311.13,
    "E": 329.63,
    "F": 349.23,
    "F#": 369.99,
    "Gb": 369.99,
    "G": 392,
    "G#": 415.3,
    "Ab": 415.3,
    "A": 440,
    "A#": 466.16,
    "Bb": 466.16,
    "B": 493.88
  };

  //Convert note to frequency based on fourth octave of the pitch the user entered
  //(because the frequency of a note in each octave is 2x the frequency in  the previous octave)
  var freq = notes[pitch]*Math.pow(2, octave - 4);
  return freq;
}

function storeFreqs() {
  
  //Store all values as variables
  let lowE = document.getElementById('lowefreq').value;
  let a = document.getElementById('afreq').value;
  let d = document.getElementById('dfreq').value;
  let g = document.getElementById('gfreq').value;
  let b = document.getElementById('bfreq').value;
  let highE = document.getElementById('highefreq').value;
  let cents = document.getElementById('freqcents').value;

  var allValues = [lowE, a, d, g, b, highE, cents];

  //Send values to Raspberry Pi
  api.put('/values', {
    values: allValues
  })
}

function submitEnables() { 
  api.put('/enables', {
    enables: string_ids.map((id) =>
      document.getElementById(`${id}enable`).checked
     )
  })
}

function enablesSetAll(state) {
  for (let id of string_ids) {
    document.getElementById(`${id}enable`).checked = state
  }
}


export default App

